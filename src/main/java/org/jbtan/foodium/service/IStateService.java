package org.jbtan.foodium.service;

import java.util.List;
import org.jbtan.foodium.dto.StateDto;

public interface IStateService {

  List<StateDto> getStateList();

}
