package org.jbtan.foodium.service;

import java.util.List;
import org.jbtan.foodium.dto.FoodiumDto;

public interface IFoodiumService {

  List<FoodiumDto> getFoodiumList();

  void saveFoodium(FoodiumDto foodiumDto);

  void updateFoodium(FoodiumDto foodiumDto);
}
