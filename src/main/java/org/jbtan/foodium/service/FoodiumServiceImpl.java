package org.jbtan.foodium.service;

import java.util.ArrayList;
import java.util.List;
import org.jbtan.foodium.dto.FoodiumDto;
import org.jbtan.foodium.entity.Foodium;
import org.jbtan.foodium.repository.IFoodiumRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FoodiumServiceImpl implements IFoodiumService {

  @Autowired
  private IFoodiumRepository foodiumRepository;

  @Autowired
  private ModelMapper modelMapper;

  @Override
  public List<FoodiumDto> getFoodiumList() {

    List<FoodiumDto> foodiumDtoList = new ArrayList<FoodiumDto>();
    List<Foodium> foodiumList = foodiumRepository.findAll();

    for (Foodium foodium : foodiumList) {
      FoodiumDto foodiumDto = modelMapper.map(foodium, FoodiumDto.class);
      foodiumDtoList.add(foodiumDto);
    }

    return foodiumDtoList;
  }

  @Override
  public void saveFoodium(FoodiumDto foodiumDto) {

    Foodium foodium = modelMapper.map(foodiumDto, Foodium.class);

    foodiumRepository.save(foodium);

  }

  @Override
  public void updateFoodium(FoodiumDto foodiumDto) {

    foodiumRepository.findById((long) foodiumDto.getId()).map(existing -> {
      existing.setDescr(foodiumDto.getDescr());
      existing.setFavourite(foodiumDto.getFavourite());
      existing.setFileName(foodiumDto.getFileName());
      existing.setFilePath(foodiumDto.getFilePath());
      return foodiumRepository.save(existing);
    });

  }


}
