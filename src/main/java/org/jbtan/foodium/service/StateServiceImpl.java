package org.jbtan.foodium.service;

import java.util.ArrayList;
import java.util.List;
import org.jbtan.foodium.dto.StateDto;
import org.jbtan.foodium.entity.State;
import org.jbtan.foodium.repository.IStateRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StateServiceImpl implements IStateService {

  @Autowired
  private IStateRepository stateRepository;

  @Autowired
  private ModelMapper modelMapper;

  @Override
  public List<StateDto> getStateList() {
    List<StateDto> stateDtoList = new ArrayList<StateDto>();
    List<State> stateList = stateRepository.findAll();

    for (State state : stateList) {
      StateDto stateDto = modelMapper.map(state, StateDto.class);

      stateDtoList.add(stateDto);
    }
    return stateDtoList;
  }

}
