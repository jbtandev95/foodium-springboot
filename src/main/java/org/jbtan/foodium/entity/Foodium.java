package org.jbtan.foodium.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "foodium")
public class Foodium {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id")
  private int id;

  @Column(name = "foodium_nm")
  private String foodiumName;

  @Column(name = "descr")
  private String descr;

  @Column(name = "file_path")
  private String filePath;

  @Column(name = "state_id")
  private int stateId;

  @Column(name = "tags")
  private String tags;

  @Column(name = "file_nm")
  private String fileName;

  @Column(name = "favourite")
  private String favourite;

  @Column(name = "price_range")
  private String priceRange;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getFoodiumName() {
    return foodiumName;
  }

  public void setFoodiumName(String foodiumName) {
    this.foodiumName = foodiumName;
  }

  public String getDescr() {
    return descr;
  }

  public void setDescr(String descr) {
    this.descr = descr;
  }

  public String getFilePath() {
    return filePath;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  public int getStateId() {
    return stateId;
  }

  public void setStateId(int stateId) {
    this.stateId = stateId;
  }

  public String getTags() {
    return tags;
  }

  public void setTags(String tags) {
    this.tags = tags;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public String getFavourite() {
    return favourite;
  }

  public void setFavourite(String favourite) {
    this.favourite = favourite;
  }

  public String getPriceRange() {
    return priceRange;
  }

  public void setPriceRange(String priceRange) {
    this.priceRange = priceRange;
  }


}
