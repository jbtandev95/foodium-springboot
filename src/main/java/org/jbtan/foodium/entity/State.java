package org.jbtan.foodium.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "states")
public class State {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "state_id")
  private int stateId;

  @Column(name = "state_cd")
  private String stateCode;

  @Column(name = "state_nm")
  private String stateName;

  public int getStateId() {
    return stateId;
  }

  public void setStateId(int stateId) {
    this.stateId = stateId;
  }

  public String getStateCode() {
    return stateCode;
  }

  public void setStateCode(String stateCode) {
    this.stateCode = stateCode;
  }

  public String getStateName() {
    return stateName;
  }

  public void setStateName(String stateName) {
    this.stateName = stateName;
  }

  @Override
  public String toString() {
    return String.format("State[stateId=%d, stateCode='%s', stateName='%s']", stateId, stateCode,
        stateName);
  }
}
