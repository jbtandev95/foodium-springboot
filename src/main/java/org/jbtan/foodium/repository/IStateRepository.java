package org.jbtan.foodium.repository;

import org.jbtan.foodium.entity.State;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface IStateRepository extends JpaRepository<State, Long> {

}
