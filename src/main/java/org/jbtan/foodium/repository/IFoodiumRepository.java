package org.jbtan.foodium.repository;

import org.jbtan.foodium.entity.Foodium;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface IFoodiumRepository extends JpaRepository<Foodium, Long> {

}
