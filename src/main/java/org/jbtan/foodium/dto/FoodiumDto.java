package org.jbtan.foodium.dto;

import java.io.Serializable;

public class FoodiumDto implements Serializable {

  private int id;
  private String foodiumName;
  private String descr;
  private String filePath;
  private int stateId;
  private String tags;
  private String fileName;
  private String favourite;
  private String priceRange;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getFoodiumName() {
    return foodiumName;
  }

  public void setFoodiumName(String foodiumName) {
    this.foodiumName = foodiumName;
  }

  public String getDescr() {
    return descr;
  }

  public void setDescr(String descr) {
    this.descr = descr;
  }

  public String getFilePath() {
    return filePath;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  public int getStateId() {
    return stateId;
  }

  public void setStateId(int stateId) {
    this.stateId = stateId;
  }

  public String getTags() {
    return tags;
  }

  public void setTags(String tags) {
    this.tags = tags;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public String getFavourite() {
    return favourite;
  }

  public void setFavourite(String favourite) {
    this.favourite = favourite;
  }

  public String getPriceRange() {
    return priceRange;
  }

  public void setPriceRange(String priceRange) {
    this.priceRange = priceRange;
  }

}
