package org.jbtan.foodium.resource;

import java.util.List;
import org.jbtan.foodium.dto.StateDto;
import org.jbtan.foodium.service.IStateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/state")
public class StateResource {

  Logger logger = LoggerFactory.getLogger(StateResource.class);

  @Autowired
  private IStateService stateService;

  @GetMapping("/all")
  public ResponseEntity<List<StateDto>> getStateList() {

    logger.info("Hello im here");

    List<StateDto> stateDtoList = stateService.getStateList();

    return new ResponseEntity<List<StateDto>>(stateDtoList, null, HttpStatus.OK);
  }
}
