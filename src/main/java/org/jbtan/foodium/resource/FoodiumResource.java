package org.jbtan.foodium.resource;

import java.util.List;
import org.jbtan.foodium.dto.FoodiumDto;
import org.jbtan.foodium.req.FoodiumRequestBody;
import org.jbtan.foodium.service.IFoodiumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/foodium")
public class FoodiumResource {

  @Autowired
  private IFoodiumService foodiumService;

  @GetMapping("/all")
  public ResponseEntity<List<FoodiumDto>> getFoodiumList() {

    List<FoodiumDto> foodiumDtoList = foodiumService.getFoodiumList();

    return new ResponseEntity<List<FoodiumDto>>(foodiumDtoList, HttpStatus.OK);
  }

  @PostMapping("/create")
  public ResponseEntity<FoodiumDto> saveNewEntry(@RequestBody FoodiumRequestBody request) {

    foodiumService.saveFoodium(request.getFoodium());
    return new ResponseEntity<FoodiumDto>(request.getFoodium(), HttpStatus.OK);
  }

  @PutMapping("/update/{id}")
  public ResponseEntity<FoodiumDto> updateEntry(@RequestBody FoodiumRequestBody request,
      @PathVariable Integer id) {

    foodiumService.updateFoodium(request.getFoodium());

    return new ResponseEntity<FoodiumDto>(request.getFoodium(), HttpStatus.OK);
  }

  @DeleteMapping("/delete/{id}")
  public ResponseEntity<FoodiumDto> deleteEntry(@PathVariable Integer id) {

    foodiumService.deleteFoodium(id);

    return new ResponseEntity<FoodiumDto>(HttpStatus.OK);
  }
}
