package org.jbtan.foodium.req;

import java.io.Serializable;
import org.jbtan.foodium.dto.FoodiumDto;

public class FoodiumRequestBody implements Serializable {

  public FoodiumDto foodium;

  public FoodiumDto getFoodium() {
    return foodium;
  }

  public void setFoodium(FoodiumDto foodium) {
    this.foodium = foodium;
  }

}
