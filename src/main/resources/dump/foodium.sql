-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: jb_enterprise
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `foodium`
--

DROP TABLE IF EXISTS `foodium`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `foodium` (
  `id` int NOT NULL AUTO_INCREMENT,
  `foodium_nm` varchar(50) NOT NULL,
  `state_id` int DEFAULT NULL,
  `file_path` varchar(200) DEFAULT NULL,
  `descr` varchar(200) DEFAULT NULL,
  `tags` varchar(200) DEFAULT NULL,
  `file_nm` varchar(50) DEFAULT NULL,
  `favourite` varchar(1) DEFAULT NULL,
  `price_range` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `state_id` (`state_id`),
  CONSTRAINT `foodium_ibfk_1` FOREIGN KEY (`state_id`) REFERENCES `states` (`state_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `foodium`
--

LOCK TABLES `foodium` WRITE;
/*!40000 ALTER TABLE `foodium` DISABLE KEYS */;
INSERT INTO `foodium` VALUES (1,'Strangers at 47',12,'../foodium-file-repository/1580278328866_Strangers_at_47.jpg','2','western,crepe,cafe','1580278328866_Strangers_at_47.jpg','Y',NULL),(2,'Jeq In The House',12,'../foodium-file-repository/1580280259193_Jeq_in_the_house.JPG','2','western,cafe','1580280259193_Jeq_in_the_house.JPG','N',NULL),(3,'Nomms',12,'../foodium-file-repository/1580284851951_nomms.jpg','2','fried chicken','1580284851951_nomms.jpg','Y',NULL),(4,'Shokudo Japanese Curry Rice',12,'../foodium-file-repository/1580292728534_shokudo-1.jpg','1','curry,japanese','1580292728534_shokudo-1.jpg','Y',NULL),(5,'Awesome Canteen',12,'../foodium-file-repository/1580297575182_Awesome-Canteen-Petaling-Jaya.jpg','2','western,burger','1580297575182_Awesome-Canteen-Petaling-Jaya.jpg','Y',NULL),(6,'After Black',12,'../foodium-file-repository/1580298888941_AfterBlack.jpg','2','western,cafe','1580298888941_AfterBlack.jpg','Y',NULL),(7,'Thai Boran SS14',12,'../foodium-file-repository/1585556147124_thaiBoran.jpg','1','thai,asian','1585556147124_thaiBoran.jpg','N',NULL),(8,'GangNam Oppa BBQ',12,'../foodium-file-repository/1585556609847_gangnamoppa.jpg','2','korean,bbq,asian','1585556609847_gangnamoppa.jpg','Y',NULL),(9,'Butcher\'s Table',12,'../foodium-file-repository/1585559374719_crave3sept-butcher10.jpg','2','western,cafe','1585559374719_crave3sept-butcher10.jpg','Y',NULL);
/*!40000 ALTER TABLE `foodium` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (1);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `states` (
  `state_id` int NOT NULL AUTO_INCREMENT,
  `state_cd` varchar(3) NOT NULL,
  `state_nm` varchar(20) NOT NULL,
  `state_code` varchar(255) DEFAULT NULL,
  `state_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`state_id`),
  UNIQUE KEY `state_cd` (`state_cd`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `states`
--

LOCK TABLES `states` WRITE;
/*!40000 ALTER TABLE `states` DISABLE KEYS */;
INSERT INTO `states` VALUES (1,'JHR','Johor',NULL,NULL),(2,'KDH','Kedah',NULL,NULL),(3,'KTN','Kelantan',NULL,NULL),(4,'MLK','Melaka',NULL,NULL),(5,'NSN','Negeri Sembilan',NULL,NULL),(6,'PHG','Pahang',NULL,NULL),(7,'PNG','Penang',NULL,NULL),(8,'PRK','Perak',NULL,NULL),(9,'PLS','Perlis',NULL,NULL),(10,'SBH','Sabah',NULL,NULL),(11,'SWK','Sarawak',NULL,NULL),(12,'SGR','Selangor',NULL,NULL),(13,'TRG','Terrenganu',NULL,NULL),(14,'KUL','Kuala Lumpur',NULL,NULL),(15,'PJY','Putrajaya',NULL,NULL);
/*!40000 ALTER TABLE `states` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-08 23:16:48
